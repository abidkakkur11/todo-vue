const vm = new Vue({
    el: '#app',
  
    data: {
  
      newTodo: '',
  
      todos: [{text:"Do something",completed:false}] 
    },
  
  
  
    computed: {
  
      itemsDone() {
        return this.todos.filter(todo => todo.completed);
      },
  
      itemsTodo() {
        return this.todos.filter(todo => !todo.completed);
      } },
  
  
    methods: {
  
      addTodo() {
        const newEntry = {
          text: this.newTodo,
          completed: false 
        };
  
        if (this.newTodo.length) {
          this.todos.push(newEntry);
          this.newTodo = '';
        }
        else{
            alert("Empty..!");
        }
      },
  
      removeTodo(index) {
        this.todos.splice(index, 1);
      },
  
      clearCompleted() {
        this.todos = this.itemsTodo;
      }
     }
});